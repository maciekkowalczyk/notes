package pl.drkicek.demoapplication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pl.drkicek.demoapplication.repo.SingleNoteRepo;



@Component
public class SingleNoteManager {

    private SingleNoteRepo singleNoteRepo;

    @Autowired
    public SingleNoteManager(SingleNoteRepo singleNoteRepo) {this.singleNoteRepo = singleNoteRepo;}


//    @EventListener(ApplicationReadyEvent.class)
//    public void init(){
//
//        SingleNote note1 = new SingleNote("MK", "test1", "dupa1");
//        SingleNote note2 = new SingleNote("MK", "test2", "dupa2");
//        SingleNote note3 = new SingleNote("MKT", "test3", "dupa3");
//        singleNoteRepo.save(note1);
//        singleNoteRepo.save(note2);
//        singleNoteRepo.save(note3);
//
//
//    }

}
