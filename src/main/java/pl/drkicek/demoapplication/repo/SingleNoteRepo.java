package pl.drkicek.demoapplication.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.drkicek.demoapplication.model.SingleNote;

import java.util.List;


@Repository
public interface SingleNoteRepo extends JpaRepository<SingleNote, Long> {



}
