package pl.drkicek.demoapplication.gui;


import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import pl.drkicek.demoapplication.model.SingleNote;
import pl.drkicek.demoapplication.repo.AppUserRepo;
import pl.drkicek.demoapplication.repo.SingleNoteRepo;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.html.Label;


import java.util.List;

@Route("notes")
public class NotesGui extends VerticalLayout {

    private SingleNoteRepo singleNoteRepo;


    @Autowired
    public NotesGui(SingleNoteRepo singleNoteRepo){

        this.singleNoteRepo = singleNoteRepo;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String currentUser = auth.getName();

        List<SingleNote> allNotes = singleNoteRepo.findAll();
        //allNotes.removeIf(note -> (!note.getNoteOwner().equals(currentUser)));

        Grid<SingleNote> grid = new Grid<>(SingleNote.class, false);
        grid.addColumn(SingleNote::getNoteOwner).setHeader("Note owner");
        grid.addColumn(SingleNote::getNoteTitle).setHeader("Note title");
        grid.addColumn(SingleNote::getNoteContent).setHeader("Note Content");


        grid.setItems(allNotes);



        Label userlabel = new Label();
        userlabel.setText("Welcome: " + currentUser);


        add(userlabel);
        add(grid);



    }

}
