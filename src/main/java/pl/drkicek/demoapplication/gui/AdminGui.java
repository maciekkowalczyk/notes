package pl.drkicek.demoapplication.gui;


import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import pl.drkicek.demoapplication.repo.AppUserRepo;

@Route("admin")
public class AdminGui extends VerticalLayout {

    private AppUserRepo appUserRepo;

    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    String currentUser = auth.getName();

    @Autowired
    public AdminGui(AppUserRepo appUserRepo) {
        this.appUserRepo = appUserRepo;

        Label label1 = new Label();
        label1.setText(currentUser);

        add(label1);

    }




}
