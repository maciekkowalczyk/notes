package pl.drkicek.demoapplication;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import pl.drkicek.demoapplication.repo.AppUserRepo;



@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    public UserDetailsServiceImpl userDetailsServiceImpl;
    public AppUserRepo appUserRepo;

    @Autowired
    public WebSecurityConfig(UserDetailsServiceImpl userDetailsServiceImpl, AppUserRepo appUserRepo) {
        this.userDetailsServiceImpl = userDetailsServiceImpl;
        this.appUserRepo = appUserRepo;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsServiceImpl);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/notes").hasAnyRole("USER", "ADMIN")
                .antMatchers("/admin").hasRole( "ADMIN")
                .and()
                .formLogin().permitAll()
                .and()
                .csrf().disable();
    }

    @Bean
    public PasswordEncoder passwordEncoder()
    {
        return new BCryptPasswordEncoder();
    }

//    @EventListener(ApplicationReadyEvent.class)
//    public void initUser(){
//        AppUser appUser = new AppUser("user", passwordEncoder().encode("user"), "ROLE_USER");
//        AppUser appAdmin = new AppUser("admin", passwordEncoder().encode("admin"), "ROLE_ADMIN");
//        appUserRepo.save(appUser);
//        appUserRepo.save(appAdmin);
//
//
//    }

}
